
# OpenADR plugin for GridLAB-D

This project implements a new add-on for *GridLAB-D*, a power system simulation tool provided by the Department of Energy (DOE) of U.S.

This new add-on provides a way to test OpenADR programs based on simple signals. The project is designed so that behaviors in response to events can be easily added and configured.

This project has been written in [C++11](https://isocpp.org/wiki/faq/cpp11).


## Functional description

The basic operation of the add-on consists of the following steps:

 1. Events defined in the program (expressed through a JSON file) are received by an entity
 named `OadrCenter`. This works as a hub node and is responsible for transmitting each input event (or the event resulting from processing the original input event) to the end-nodes involved in the demand-response program.

 2. The end-nodes, which contain subelements of type `OadrBox`, receive the DR events and act accordingly. The `OadrBox` comes to represent the part of the AMI (_Advanced Metering Infrastructure_) that receives and processes the OpenADR events. This is intended to be part of each household involved in the DR program. 

The current version of the add-on includes some behaviors for both types of elements. In particular, the behaviors related to the element `OadrCenter` decide how the input event is conveyed to the end-nodes, as well as which of them should receive it; while the behavior related to the `OadrBox` decides how nodes react to specific demand-response events. In the source code, the behaviors corresponding to the `OadrCenter` are those that extend the base class `CenterPlanner`; while the behaviors corresponding to `OadrBox` extend the class `EventProcessor`.


## GLM elements

In order to use the _openadr_ plugin, GLM files must include the following elements:

 - Module _openadr_. This includes the parameters:
    * _program_: Path to the file that defines the demand-response program.
    * _eventProcessor_: Name of the behavior that the `OadrBox` element must use when an event is received.

 - Top-level element `OadrCenter`. This includes the parameters:
    * _name_: Name of the element. This is useful in order for the element can be referred by the OadrBoxes.
    * _plannerName_: Name of the behavior that the `OadrCenter` must use when an event is received.
    * _dbase_: Reference to an element of type database. The database can be used to store information required by the _planner_ (behavior of OadrCenters).

 - Sub-elements of type `OadrBox`. Each household participating in the demand-response program must include an OadrBox. This includes the parameter:
    * _name_: Name of the element.


The folder `resources/glm` contains some GLM sample files.


## Installation

This project depends on the extra libraries `json-c` and `mysql-connector-c`, so both of them must be installed on the system.

The folder containing this project must be copied into the GridLAB-D sources folder **using the name** `openadr`. Moreover, the following line must be added to the file `Makefile.am`:

    include openadr/Makefile.mk

Then GridLAB-D can be compiled and installed as usual. Note that support for _MySQL_ must be enabled during the configuration and compilation of GridLAB-D.

To check if the module has been installed properly, you can type:

    $ gridlabd -L openadr

This project can also be compiled independently by using [CMake](https://cmake.org/). To this end, the file `CMakeLists.txt` is provided.


## License

The add-on _opeandr_ is free software: you can redistribute it and/or modify it under the terms of the [**GNU General Public License**](http://www.gnu.org/licenses/gpl.html) as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

The add-on _opeandr_ is distributed in the hope that it will be useful, but **without any warranty**; without even the implied warranty of **merchantability** or **fitness for a particular purpose**.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).

The add-on _opeandr_ is copyright (c) 2017 Ignacio Lopez-Rodriguez.
