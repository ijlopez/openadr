#include <cstdlib>
#include <iostream>
#include <sstream>
#include "CenterPlannerSmart.h"
#include "CenterPlannerFactory.h"
#include "OadrBox.h"

namespace oadr {
    std::string CenterPlannerSmart::name = "smart";
    const bool CenterPlannerSmart::registered =
            CenterPlannerFactory::registerCenterPlanner(name, &CenterPlannerSmart::create);

    CenterPlannerSmart::CenterPlannerSmart(PlannerData data) : CenterPlanner(data) {
        return;
    }

    CenterPlannerSmart::~CenterPlannerSmart() {
        return;
    }

    void CenterPlannerSmart::run() {
        if (data.event->getStartAfter() == 0) { // if there isn't chance to play with 'startAfter'.
            gl_warning("CenterPlannerSmart: run: Using the default planner's implementation.");
            defaultRun(data);
            return;
        }

        // Read information (from the database) on the houses' HVAC load.
        std::vector<DbItem> *items = buildDbItemsArray();
        if (items == NULL) {
            gl_debug("CenterPlannerSmart: failed to create the array of db-items.");
            return;
        }
        const unsigned long n = items->size();
        const int d = data.event->getStartAfter();
        const long s = data.event->getStartTime();
        for (std::vector<OBJECT*>::const_iterator it = data.boxes->begin(); it != data.boxes->end(); it++) {
            OadrBox *box = OBJECTDATA(*it, OadrBox);
            if (box == NULL) {
                continue;
            }
            FUNCTIONADDR func_reg_event = box->get_function("register_event");

            const DbItem *dbItem = NULL;
            for (std::vector<DbItem>::const_iterator et = items->begin(); et != items->end(); et++) {
                if ((et->name.c_str() == NULL) || (box->get_parent()->get_name() == NULL)) {
                    gl_debug("CenterPlannerSmart: the name of the item is NULL.");
                    break;
                }
                if (strcmp(et->name.c_str(), box->get_parent()->get_name()) == 0) {
                    dbItem = et.base();
                    break;
                }
            }

            if (dbItem == NULL) { // there isn't previous data for this house object. The event is sent as it is.
                gl_error("CenterPlannerSmart: item without DB history!: %s", box->get_parent()->get_name());
                func_reg_event(*it, &data.event);
            } else {
                // There is database information on this object.
                const long i = dbItem->idx;
                const long newStartTime = s + (long)((i%2 == 0)? roundf(d*i/(float)n) : roundf(d*(n - i)/(float)n));
                oadr::ProgramEvent boxEvent(
                        newStartTime,
                        data.event->getNotifDuration(),
                        data.event->getDuration(),
                        data.event->getRecoveryDuration(),
                        data.event->getLevel(),
                        0);
                func_reg_event(*it, &boxEvent);
            }
        }

        delete items;

        return;
    }

    CenterPlanner* CenterPlannerSmart::create(PlannerData data) {
        return new CenterPlannerSmart(data);
    }

    std::vector<DbItem>* CenterPlannerSmart::buildDbItemsArray() {

        MYSQL *conn = mysql_init(NULL);
        if (conn == NULL) {
            gl_error("CenterPlannerSmart: buildDbItemsArray: failed to initialize the MySQL object.");
            return NULL;
        }

        database *dbase = data.dbase;
        conn = mysql_real_connect(conn,
                           dbase->get_hostname(),
                           dbase->get_username(),
                           strcmp(dbase->get_password(), "") == 0? NULL : dbase->get_password(),
                           dbase->get_schema(),
                           dbase->get_port(),
                           NULL,
                           0);
        if (conn == NULL) {
            gl_error("CenterPlannerSmart: buildDbItemsArray: failed to establish the MySQL connection.");
            return NULL;
        }

        // Read (from the database) the HVAC load of all houses.
        std::stringstream query;
        query << "SELECT name, SUM(avg_load) AS total_load FROM houses WHERE t ";
        query << "BETWEEN from_unixtime(" << data.event->getStopTime() << ") and from_unixtime(" << data.event->getStopTime() + data.event->getStartAfter() << ") ";
        query << "GROUP BY name ORDER BY total_load DESC";

        if (mysql_query(conn, query.str().c_str()) != 0) {
            gl_error("CenterPlannerSmart: buildDbItemsArray: the sql query failed to obtain the records: %s", query.str().c_str());
            mysql_close(conn);
            return NULL;
        }

        // - data is stored into a vector structure.
        MYSQL_RES *sqlRes = mysql_store_result(conn);
        std::vector<DbItem> *items = new std::vector<DbItem>(mysql_num_rows(sqlRes));
        int idx = 0;
        MYSQL_ROW row;
        while ((row = mysql_fetch_row(sqlRes)) != NULL) {
            DbItem item;
            item.hvacload = atof(row[1]);
            item.name = std::string(row[0]);
            item.idx = idx++;
            items->push_back(item);
        }

        mysql_close(conn);

        return items;
    }
}
