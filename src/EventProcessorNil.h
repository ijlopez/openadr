#ifndef OPENADR_EVENTPROCESSORNIL_H
#define OPENADR_EVENTPROCESSORNIL_H

#include "EventProcessor.h"

namespace oadr {
    class EventProcessorNil : public EventProcessor {
    public:
        virtual ~EventProcessorNil();

        virtual void process(const ProgramEvent &event, const EventProcessorData &data);

        virtual void stop(const ProgramEvent &event, const EventProcessorData &data);

        static EventProcessor *create();

    protected:
        EventProcessorNil();

    protected:
        static std::string name;
        static const bool registered;
    };
}

#endif //OPENADR_EVENTPROCESSORNIL_H
