#include <cstdlib>
#include <iostream>
#include <sstream>
#include "CenterPlannerOrder.h"
#include "CenterPlannerFactory.h"
#include "OadrBox.h"

namespace oadr {
    std::string CenterPlannerOrder::name = "order";
    const bool CenterPlannerOrder::registered =
            CenterPlannerFactory::registerCenterPlanner(name, &CenterPlannerOrder::create);

    /**
     *
     */
    CenterPlannerOrder::CenterPlannerOrder(PlannerData data) : CenterPlanner(data) {
        return;
    }

    /**
     *
     */
    CenterPlannerOrder::~CenterPlannerOrder() {
        return;
    }

    /**
     *
     */
    void CenterPlannerOrder::run() {
        if (data.event->getStartAfter() == 0) { // if there isn't chance to play with 'startAfter'.
            gl_warning("CenterPlannerOrder: run: Using the default planner's implementation.");
            defaultRun(data);
            return;
        }

        // Read information (from the database) on the houses' HVAC load.
        std::vector<DbItem> *items = buildDbItemsArray();
        if (items == NULL) {
            gl_debug("CenterPlannerOrder: failed to create the array of db-items.");
            return;
        }

        const long s = data.event->getStartTime();
        const float offsetFactor = data.event->getStartAfter() / (float)items->size();
        int i = 0;
        for (std::vector<OBJECT*>::const_iterator it = data.boxes->begin(); it != data.boxes->end(); it++, i++) {
            OadrBox *box = OBJECTDATA(*it, OadrBox);
            if (box == NULL) {
                continue;
            }
            FUNCTIONADDR func_reg_event = box->get_function("register_event");

            const DbItem *dbItem = NULL;
            for (std::vector<DbItem>::const_iterator et = items->begin(); et != items->end(); et++) {
                if (strcmp(et->name.c_str(), box->get_parent()->get_name()) == 0) {
                    dbItem = et.base();
                    break;
                }
            }

            if (dbItem == NULL) { // there isn't previous data for this house object. The event is sent as it is.
                gl_warning("CenterPlannerOrder: item without DB history!: %s", box->get_name());
                func_reg_event(*it, &data.event);
            } else {
                // There is database information on this object.
                const long newStartTime = s + (long)(i*offsetFactor);
                oadr::ProgramEvent boxEvent(
                        newStartTime,
                        data.event->getNotifDuration(),
                        data.event->getDuration(),
                        data.event->getRecoveryDuration(),
                        data.event->getLevel(),
                        0);
                func_reg_event(*it, &boxEvent);
            }
        }

        delete items;

        return;
    }

    /**
     *
     */
    CenterPlanner* CenterPlannerOrder::create(PlannerData data) {
        return new CenterPlannerOrder(data);
    }

    /**
     *
     */
    std::vector<DbItem>* CenterPlannerOrder::buildDbItemsArray() {
        MYSQL *conn = mysql_init(NULL);
        if (conn == NULL) {
            gl_error("CenterPlannerOrder: buildDbItemsArray: failed to initialize the MySQL object.");
            return NULL;
        }

        database *dbase = data.dbase;
        conn = mysql_real_connect(conn,
                                  dbase->get_hostname(),
                                  dbase->get_username(),
                                  strcmp(dbase->get_password(), "") == 0? NULL : dbase->get_password(),
                                  dbase->get_schema(),
                                  dbase->get_port(),
                                  NULL,
                                  0);
        if (conn == NULL) {
            gl_error("CenterPlannerOrder: buildDbItemsArray: failed to establish the MySQL connection.");
            return NULL;
        }

        // Read (from the database) the HVAC load of all houses.
        std::stringstream query;
        query << "SELECT name, sum(hvac_load) AS hvac_load FROM houses WHERE t ";
        query << "BETWEEN from_unixtime(" << data.event->getStartTime() << ") and from_unixtime(" << data.event->getStopTime() << ") ";
        query << "GROUP BY name ORDER BY hvac_load DESC";

        if (mysql_query(conn, query.str().c_str()) != 0) {
            gl_error("CenterPlannerOrder: buildDbItemsArray: the sql query failed to obtain the records.");
            mysql_close(conn);
            return NULL;
        }

        gl_warning("CenterPlannerOrder: SQL: %s", query.str().c_str());

        // - data is stored into a vector structure.
        MYSQL_RES *sqlRes = mysql_store_result(conn);
        std::vector<DbItem> *items = new std::vector<DbItem>(mysql_num_rows(sqlRes));
        int idx = 0;
        MYSQL_ROW row;
        while ((row = mysql_fetch_row(sqlRes)) != NULL) {
            DbItem item;
            item.hvacload = atof(row[1]);
            item.name = std::string(row[0]);
            item.idx = idx++;
            items->push_back(item);
        }

        mysql_close(conn);

        return items;
    }
}