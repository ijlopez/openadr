#ifndef OPENADR_CENTERPLANNERORDER_H
#define OPENADR_CENTERPLANNERORDER_H

#include "CenterPlanner.h"

namespace oadr {
    class CenterPlannerOrder : public CenterPlanner {
    public:
        virtual ~CenterPlannerOrder();
        virtual void run();
        static CenterPlanner* create(PlannerData data);

    protected:
        CenterPlannerOrder(PlannerData data);

    protected:
        static std::string name;
        static const bool registered;

    private:
        std::vector<DbItem>* buildDbItemsArray();

    };
}


#endif //OPENADR_CENTERPLANNERORDER_H
