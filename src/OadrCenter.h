#ifndef SRC_OADRCENTER_H_
#define SRC_OADRCENTER_H_

#include <vector>
#include "Program.h"
#include "CenterPlanner.h"
#include <gridlabd.h>
#include <database.h>

// Forward declaration of the functions published by the OadrCenter object.
EXPORT int64 register_box(OBJECT *box, OBJECT *center);

/**
 * SimulationData.
 * Stores information related to the ongoing simulation.
 */
typedef struct SData {
    int idxNextEvent;
    std::vector<OBJECT*> boxes;
} SimulationData;

/**
 * OadrCenter represents the entity responsible for raising and communicating events.
 */
class OadrCenter: public gld_object {
public: // published properties
    GL_ATOMIC(OBJECT*, dbase);
    GL_STRING(char256, plannerName);

public:
	OadrCenter(MODULE *mod);
    ~OadrCenter();
	int create(void);
	int init(OBJECT *parent);
    void finish();

    TIMESTAMP presync(TIMESTAMP t0);
    TIMESTAMP sync(TIMESTAMP t0);
    TIMESTAMP postsync(TIMESTAMP t0);
    
    void registerOadrBox(OBJECT*);

public: // required members
	static CLASS *oclass;
	static OadrCenter *defaults;

private: // internal functions
	bool checkOadrCenterUniqueness() const;
    const oadr::ProgramEvent* findNextEvent(TIMESTAMP t);
    database* getDatabaseObj();

private:
	oadr::Program *program;
    SimulationData simulData;
};

#endif /* SRC_OADRCENTER_H_ */
