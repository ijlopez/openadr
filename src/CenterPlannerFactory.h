#ifndef OPENADR_CENTERPLANNERFACTORY_H
#define OPENADR_CENTERPLANNERFACTORY_H

#include <string>
#include <map>
#include "CenterPlanner.h"


namespace oadr {

    typedef CenterPlanner* (*CenterPlannerCreateFn)(PlannerData);
    typedef std::map<std::string, CenterPlannerCreateFn> PlannersMap;

    class CenterPlannerFactory {
    public:
        static CenterPlanner* createCenterPlanner(std::string name, PlannerData pdata);

        static void free(CenterPlanner *planner);

        static bool registerCenterPlanner(const std::string name, CenterPlannerCreateFn pfnCreate);

    private:
        static PlannersMap planners;
    };
}


#endif //OPENADR_CENTERPLANNERFACTORY_H
