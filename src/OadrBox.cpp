#include <malloc.h>
#include <string>
#include <algorithm>
#include "OadrCenter.h"
#include "OadrBox.h"
#include "EventProcessorFactory.h"
#include "globals.h"


EXPORT_INIT(OadrBox);
EXPORT_CREATE(OadrBox);
EXPORT_SYNC(OadrBox);

CLASS *OadrBox::oclass = NULL;
OadrBox *OadrBox::defaults = NULL;

/**
 * Constructor.
 */
OadrBox::OadrBox(MODULE *mod) {
	if (oclass != NULL) {
		exception("OadrBox: this object has been already registered.");
		return;
	}

	oclass = gld_class::create(mod, "OadrBox", sizeof(OadrBox), PC_PRETOPDOWN|PC_BOTTOMUP|PC_POSTTOPDOWN|PC_AUTOLOCK);
	if (oclass == NULL) {
		exception((char*)"OadrBox: unable to register the class OadrBox.");
		return;
	}

	if (gl_publish_variable(
            oclass,
            PT_object, "center", get_center_offset(), PT_DESCRIPTION, "OpenADR center to which the OadrBox is attached to.",
            PT_double,"heating_setpoint[degF]", get_heatingSetpoint_offset(), PT_DESCRIPTION,"thermostat heating setpoint",
            PT_double,"cooling_setpoint[degF]", get_coolingSetpoint_offset(), PT_DESCRIPTION,"thermostat cooling setpoint",
            PT_double,"tank_setpoint[degF]", get_tankSetpoint_offset(),PT_DESCRIPTION, "waterheater setpoint",
			NULL) < 1) {
		exception((char*)"OadrBox: unable to publish its own variables.");
	}

    gl_publish_function(oclass, "register_event", (FUNCTIONADDR)register_event);

	oclass->trl = TRL_UNKNOWN;
	defaults = this;

	memset(defaults = this, 0, sizeof(OadrBox));
}


/**
 * ~OadrBox.
 * Default destructor.
 */
OadrBox::~OadrBox() {
    finish();
    return;
}


/**
 * finish.
 */
void OadrBox::finish() {
    if (this->eventProcessor != NULL) {
        oadr::EventProcessorFactory::free(this->eventProcessor);
        this->eventProcessor = NULL;
    }
    return;
}


/**
 * create.
 *
 * The create function is run before user-defined values are loaded and "init" is performed.
 * This event allows you to set up each "object" prior to user values being set by the GLM file.
 * This is the time to set default values and values used to detect whether the user defined
 * required values (e.g., negative or zero that are not valid).
 *
 * The "create" function must return SUCCESS or FAILED to indicate the result. The simulation
 * will stop if the return value is not SUCCESS.
 *
 */
int OadrBox::create(void) {
	memcpy(this, defaults, sizeof(*this));
	this->center = NULL;
    this->tankSetpoint = -1;
    this->coolingSetpoint = -1;
    this->heatingSetpoint = -1;
	return SUCCESS;
}


/**
 * init.
 *
 * Object initialization is called once after all object have been created.
 * The values specified by the user at the GLM file are loaded at this time.
 *
 */
int OadrBox::init(OBJECT *parent) {
    OBJECT *hdr = OBJECTHDR(this);
    if ((this->center == NULL) || (OBJECTDATA(this->center, OadrCenter) == NULL)) {
        exception("OadrBox: init: the box is not attached to an OadrCenter element.");
        return FAILED;
    } else if (this->my()->parent == NULL) {
        exception("OadrBox: init: the box hasn't parent element.");
        return FAILED;
    }


    if (!this->get_parent()->isa("house")) {
        exception("OadrBox: init: failed to get the house_e element the OadrBox should be attached to.");
        return FAILED;
    }

    // Register with the OadrCenter.
    OadrCenter* center = OBJECTDATA(this->center, OadrCenter);
    FUNCTIONADDR func_reg_box = center->get_function("register_box");
    func_reg_box(hdr, this->center);

    // Create and attach the EventProcessor.
    initEventProcessorData();
    try {
        this->eventProcessor =
                oadr::EventProcessorFactory::createEventProcessor(std::string(oadrEventProcessorName));
    } catch (char const *exMsg) {
        exception("OadrBox: failed to create the EventProcessor");
    }

 	return SUCCESS;
}

/**
 * sync.
 */
TIMESTAMP OadrBox::sync(TIMESTAMP t0) {
    return TS_NEVER;
}

/**
 * presync.
 *
 * This is called when the clock needs to advance on the first top-down pass.
 */
TIMESTAMP OadrBox::presync(TIMESTAMP t0) {
    if (events.size() == 0) {
        applyNormalControl();
        return TS_NEVER;
    }

    const oadr::ProgramEvent &event = events[0];
    const TIMESTAMP startTime = event.getStartTime();
    const TIMESTAMP stopTime = event.getStopTime();
    if (t0 < startTime) {
        applyNormalControl();
        return startTime;
    }

    epData.coolingSetpoint = this->coolingSetpoint;
    epData.heatingSetpoint = this->heatingSetpoint;
    epData.tankSetpoint = this->tankSetpoint;
    if ((t0 >= startTime) && (t0 < stopTime)) {
        this->eventProcessor->process(event, epData);
        return stopTime;
    } else if (t0 == stopTime) {
        this->eventProcessor->stop(event, epData);
        popEvent();
    }

    applyNormalControl();

    // Start time of the next DR event.
    long nextStartTime = nextEventStartTime();
    if (nextStartTime == -1) {
        return TS_NEVER;
    }

    return nextStartTime;
}


/**
 * postsync.
 *
 * This is called when the clock needs to advance on the second top-down pass.
 */
TIMESTAMP OadrBox::postsync(TIMESTAMP t0) {
    return TS_NEVER;
}


/*
 * registerEvent.
 */
void OadrBox::registerEvent(oadr::ProgramEvent event) {
    if (event.getStartAfter() != 0) { // the start time must be modified.
        event.applyStartAfter();
    }

    events.push_back(event);
    std::sort(
        events.begin(),
        events.end(),
        sortStartTime);

    return;
}

/**
 * nextEventStartTime.
 */
TIMESTAMP OadrBox::nextEventStartTime() const {
    if (events.size() == 0) {
        return -1;
    }

    return events[0].getStartTime();
}


/**
 * popEvent.
 */
void OadrBox::popEvent() {
    if (events.size() == 0) {
        return;
    }

    events.erase(events.begin() + 0);
    return;
}


/**
 * initEventProcessorData.
 */
void OadrBox::initEventProcessorData() {

    // fulfill the object EventProcessorData
    epData.house = OBJECTDATA(this->my()->parent, house_e);

    // - waterheater
    OBJECT *houseObj = OBJECTHDR(epData.house);
    FINDLIST *waterheaterList = gl_find_objects(FL_NEW, FT_CLASS, SAME, "waterheater", AND, FT_PARENT, FT_ID, EQ, houseObj->id, FT_END);
    epData.wheater = (waterheaterList->hit_count > 0)?
                     OBJECTDATA(gl_find_next(waterheaterList, NULL), waterheater) : NULL;

    // - zipload
    FINDLIST *zipsList = gl_find_objects(FL_NEW, FT_CLASS, SAME, "ZIPload", AND, FT_PARENT, FT_ID, EQ, houseObj->id, FT_END);
    if (zipsList->hit_count > 0) {
        epData.nZipLoads = zipsList->hit_count;
        epData.zipLoads = (OBJECT**)malloc(sizeof(OBJECT*) * epData.nZipLoads);
        OBJECT *lastObjZip = NULL;
        for (int n = 0; n < epData.nZipLoads; n++) {
            lastObjZip = gl_find_next(zipsList, lastObjZip);
            epData.zipLoads[n] = lastObjZip;
        }
    }

    return;
}

void OadrBox::applyNormalControl() {
    epData.house->cooling_setpoint = this->coolingSetpoint;
    epData.house->heating_setpoint = this->heatingSetpoint;
    if (epData.wheater != NULL) {
        epData.wheater->tank_setpoint = this->tankSetpoint;
    }

    return;
}


/**
 * register_event.
 * This is the mechanism used by OadrCenter to communicate events to the OadrBox elements.
 */
EXPORT int64 register_event(OBJECT *objBox, oadr::ProgramEvent *event) {
    if (objBox->oclass != OadrBox::oclass) {
        gl_error("register_event: event submitted to an object different to OadrBox.");
        return -1;
    }

    OadrBox *box = OBJECTDATA(objBox, OadrBox);
    box->registerEvent(*event);
    return 0;
}