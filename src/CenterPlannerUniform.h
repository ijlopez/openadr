#ifndef OPENADR_CENTERPLANNERUNIFORM_H
#define OPENADR_CENTERPLANNERUNIFORM_H

#include "CenterPlanner.h"

namespace oadr {
    class CenterPlannerUniform : public CenterPlanner {
    public:
        virtual ~CenterPlannerUniform();
        virtual void run();
        static CenterPlanner* create(PlannerData data);

    protected:
        CenterPlannerUniform(PlannerData data);

    protected:
        static std::string name;
        static const bool registered;
    };
}

#endif
