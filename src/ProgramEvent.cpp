#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include "ProgramEvent.h"
#include "utils.h"
#include <timestamp.h>
#include <gridlabd.h>


namespace oadr {

    /**
     * ProgramEvent::ProgramEvent constructor.
     * The fields 'startTime', 'notifDuration', 'duration' and 'level' must be defined when intitializing the object.
     **/
    ProgramEvent::ProgramEvent(long int startTime, int notifDuration, int duration, int recoveryDuration, OpenadrLevel level, int startAfter)
            : startTime(startTime), notifDuration(notifDuration), duration(duration),
              recoveryDuration(recoveryDuration), level(level), startAfter(startAfter) {
        this->randomized = false;
        return;
    }


    /**
     *
     */
    ProgramEvent::ProgramEvent(ProgramEvent const &other) {
        startTime = other.startTime;
        notifDuration = other.notifDuration;
        duration = other.duration;
        recoveryDuration = other.recoveryDuration;
        level = other.level;
        startAfter = other.startAfter;
        randomized = other.randomized;
        return;
    }


    /**
     *
     */
    ProgramEvent& ProgramEvent::operator=(const ProgramEvent &other) {
        if (this == &other) {
            return *this;
        }

        startTime = other.startTime;
        notifDuration = other.notifDuration;
        duration = other.duration;
        recoveryDuration = other.recoveryDuration;
        level = other.level;
        startAfter = other.startAfter;
        randomized = other.randomized;

        return *this;
    }


    /**
     * ProgramEvent::toString serializes the event's fields in JSON format.
     */
    std::string const ProgramEvent::toString() const {
        std::stringstream ss;
        ss << "{\"startTime\":" << this->startTime;
        ss << ", \"notifDuration\":" << this->notifDuration;
        ss << ", \"duration\":" << this->duration;
        ss << ", \"recoveryDuration\":" << this->recoveryDuration;
        ss << ", \"level\":" << this->level;
        ss << ", \"startAfter\":" << this->startAfter;
        ss << "}";
        return ss.str();
    }


    /**
     * ProgramEvent::buildFromJson creates an object of type ProgramEvent from a
     * JSON object. The data type of the input JSON object is typical of the
     * json-c library.
     */
    ProgramEvent ProgramEvent::buildFromJson(json_object* jsonObj) {
        long startTime;
        int notifDuration, duration, recoveryDuration, startAfter;
        OpenadrLevel oadrLevel;

        struct json_object_iterator it = json_object_iter_begin(jsonObj);
        struct json_object_iterator itEnd = json_object_iter_end(jsonObj);
        while (!json_object_iter_equal(&it, &itEnd)) {
            std::string key = std::string(json_object_iter_peek_name(&it));
            if (key.compare("duration") == 0) {
                duration = json_object_get_int(json_object_iter_peek_value(&it));

            } else if (key.compare("level") == 0) {
                std::string token = json_object_get_string(json_object_iter_peek_value(&it));
                oadrLevel = stringToOpenadrLevel(token);
                if (oadrLevel == OADR_UNKNOWN) {
                    throw "the 'level' object does not contain a valid token.";
                }

            } else if (key.compare("startTime") == 0) {
                //startTime = xmlDateToTimestamp(json_object_get_string(json_object_iter_peek_value(&it)));
                startTime = callback->time.convert_to_timestamp(
                        (char*)json_object_get_string(json_object_iter_peek_value(&it)));
                if ((startTime == TS_NEVER) || (startTime == TS_INVALID)) {
                    throw "the 'startTime' object is not formatted correctly.";
                }

            } else if (key.compare("notifDuration") == 0) {
                notifDuration = json_object_get_int(json_object_iter_peek_value(&it));

            } else if (key.compare("recoveryDuration") == 0) {
                recoveryDuration = json_object_get_int(json_object_iter_peek_value(&it));

            } else if (key.compare("startAfter") == 0) {
                startAfter = json_object_get_int(json_object_iter_peek_value(&it));
            }

            json_object_iter_next(&it);
        }

        return ProgramEvent(startTime, notifDuration, duration, recoveryDuration, oadrLevel, startAfter);
    }

    /**
     * applyStartAfter.
     * If the parameter startAfter is greater than zero, then the start time is changed.
     */
void ProgramEvent::applyStartAfter() {
    if (randomized) {
        throw "the event has already been randomized";
    } else if (startAfter == 0) {
        return;
    }

    startTime += rand() % startAfter + 1;
    randomized = true;
    return;
}


/**
 * ProgramEvent::stringToOpenadrLevel converts a string token into value of type OpenadrLevel.
 * Valid string values are: normal, moderate, high and critical.
 */
OpenadrLevel ProgramEvent::stringToOpenadrLevel(std::string token) {
    if (token.compare("normal") == 0) {
        return OADR_NORMAL;
    } else if (token.compare("moderate") == 0) {
        return OADR_MODERATE;
    } else if (token.compare("high") == 0) {
        return OADR_HIGH;
    } else if (token.compare("critical") == 0) {
        return OADR_CRITICAL;
    }

    return OADR_UNKNOWN;
}
}
