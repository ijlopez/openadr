#ifndef SRC_PROGRAMEVENT_H_
#define SRC_PROGRAMEVENT_H_

#include <json-c/json.h>
#include "Program.h"

namespace oadr {

/** Possible OpenAdr values that an OpenADR event can get. */
enum OpenadrLevel {
    OADR_UNKNOWN = -1,
    OADR_NORMAL,
    OADR_MODERATE,
    OADR_HIGH,
    OADR_CRITICAL
};

class ProgramEvent {

public:
    ProgramEvent(long startTime, int notifDuration, int duration, int recoveryDuration, OpenadrLevel level, int startAfter);

    ProgramEvent(ProgramEvent const &);

    ProgramEvent& operator=(const ProgramEvent&);

    inline long getStartTime() const {
        return startTime;
    }

    inline long getStopTime() const {
        return startTime + duration;
    }
    
    inline int getNotifDuration() const {
        return notifDuration;
    }
    
    inline int getNotifTime() const {
        return startTime - notifDuration;
    }
    
    inline int getDuration() const {
        return duration;
    }

    inline int getRecoveryDuration() const {
        return recoveryDuration;
    }

    inline int getRecoveryEnd() const {
        return getStopTime() + recoveryDuration;
    }
    
    inline OpenadrLevel getLevel() const {
        return level;
    }
    
    inline int getStartAfter() const {
        return startAfter;
    }

    void applyStartAfter();
    
    std::string const toString() const;

private:
    friend class Program;
    static inline bool sortNotifTime(ProgramEvent &e1, ProgramEvent &e2) {
        return (e1.startTime - e1.notifDuration) < (e2.startTime - e2.notifDuration);
    }
    static ProgramEvent buildFromJson(json_object* jsonObj);
    static OpenadrLevel stringToOpenadrLevel(std::string);


private:
    long startTime; /** date at which the event starts. */
    int notifDuration; /** time slot within which events must be notified. */
    int duration; /** duration of the event in seconds. */
    int recoveryDuration; /** duration of the recovery stage. */
    OpenadrLevel level; /** OpenAdr level of the simple event. */
    int startAfter; /** start time offset. */
    bool randomized; /** tells if the startTime was already modified according to startAfter. */
};

}

#endif /* SRC_PROGRAMEVENT_H_ */
