#include "CenterPlannerFactory.h"

namespace oadr {

    PlannersMap CenterPlannerFactory::planners;

    /**
     * createCenterPlanner.
     * This instantiates an object of type CenterPlanner based on the parameter 'name'.
     * The instance returned can be of any class that heritances from CenterPlanner.
     */
    CenterPlanner *CenterPlannerFactory::createCenterPlanner(std::string name, PlannerData data) {
        PlannersMap::iterator it = planners.find(name);
        if (it == planners.end()) {
            throw "planner not registered";
        }

        CenterPlannerCreateFn fn = (*it).second;
        return fn(data);
    }


    /**
     * free.
     * Releases the CenterPlanner instance.
     */
    void CenterPlannerFactory::free(CenterPlanner *planner) {
        if (planner == NULL) {
            return;
        }

        delete planner;
        return;
    }


    /**
     * registerCenterPlanner.
     * This registers a new method for creating CenterPlanner entities.
     */
    bool CenterPlannerFactory::registerCenterPlanner(const std::string name, CenterPlannerCreateFn pfnCreate) {
        planners[name] = pfnCreate;
        return true;
    }
}