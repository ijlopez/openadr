#include "EventProcessorHvac.h"
#include "EventProcessorFactory.h"

namespace oadr {
    const double HVAC_COOLING_BASEPOINT_MODERATE = 78;
    const double TANK_BASEPOINT_MODERATE = 122;

    std::string EventProcessorHvac::name = "hvac";
    const bool EventProcessorHvac::registered =
        EventProcessorFactory::registerProcessor(name, &EventProcessorHvac::create);

    /**
     * Constructor.
     */
    EventProcessorHvac::EventProcessorHvac() {
        return;
    }

    /**
     * Default destructor.
     */
    EventProcessorHvac::~EventProcessorHvac() {
        return;
    }

    /**
     * create.
     */
    EventProcessor* EventProcessorHvac::create() {
        return new EventProcessorHvac();
    }

    /**
     * process.
     */
    void EventProcessorHvac::process(const ProgramEvent &event, const EventProcessorData &data) {
        const OpenadrLevel level = event.getLevel();
        if (level == OADR_MODERATE) {
            if (data.coolingSetpoint < HVAC_COOLING_BASEPOINT_MODERATE) {
                data.house->cooling_setpoint = HVAC_COOLING_BASEPOINT_MODERATE;
            }
            data.house->heating_setpoint = data.heatingSetpoint;

            if (data.tankSetpoint > TANK_BASEPOINT_MODERATE) {
                data.wheater->tank_setpoint = TANK_BASEPOINT_MODERATE;
            }

            return;
        }

        return;
    }

    /**
     * stop.
     */
    void EventProcessorHvac::stop(const ProgramEvent &event, const EventProcessorData &data) {
        return;
    }
}