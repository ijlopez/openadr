
#include "EventProcessorNil.h"
#include "EventProcessorFactory.h"

namespace oadr {

    // Definition of the static declarations
    std::string EventProcessorNil::name = "nil";
    const bool EventProcessorNil::registered =
            EventProcessorFactory::registerProcessor(name, &EventProcessorNil::create);

    /**
     * Constructor.
     */
    EventProcessorNil::EventProcessorNil() {
        return;
    }

    /**
     * Default destructor.
     */
    EventProcessorNil::~EventProcessorNil() {
        return;
    }

    /**
     * create.
     */
    EventProcessor* EventProcessorNil::create() {
        return new EventProcessorNil();
    }

    /**
     * process.
     */
    void EventProcessorNil::process(const ProgramEvent &event, const EventProcessorData &data) {
        return;
    }

    /**
     * stop.
     */
    void EventProcessorNil::stop(const ProgramEvent &event, const EventProcessorData &data) {
        return;
    }
}
