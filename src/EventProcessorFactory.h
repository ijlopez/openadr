#ifndef OPENADR_EVENTPROCESSORFACTORY_H
#define OPENADR_EVENTPROCESSORFACTORY_H

#include <string>
#include <map>
#include "EventProcessor.h"

namespace oadr {

    typedef EventProcessor* (*EventProcessorCreateFn)();
    typedef std::map<std::string, EventProcessorCreateFn> ProcessorsMap;

    class EventProcessorFactory {
    private:
        EventProcessorFactory() {};
        EventProcessorFactory(const EventProcessorFactory &) {};
        EventProcessorFactory &operator=(const EventProcessorFactory &) {return *this;}

    public:
        static EventProcessor* createEventProcessor(std::string name);

        static void free(EventProcessor*);

        static bool registerProcessor(const std::string name, EventProcessorCreateFn pfnCreate);

    private:
        static ProcessorsMap processors;
    };
}


#endif //OPENADR_EVENTPROCESSORFACTORY_H
