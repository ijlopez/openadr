#include <string.h>
#include <string>
#include "OadrCenter.h"
#include "globals.h"
#include "CenterPlannerFactory.h"
#include <gridlabd.h>


EXPORT_INIT(OadrCenter);
EXPORT_CREATE(OadrCenter);
EXPORT_SYNC(OadrCenter);

CLASS *OadrCenter::oclass = NULL;
OadrCenter *OadrCenter::defaults = NULL;
static PASSCONFIG passconfig = PC_PRETOPDOWN|PC_POSTTOPDOWN;
static PASSCONFIG clockpass = PC_POSTTOPDOWN;

/**
 * Constructor.
 * Register the class with the core
 */
OadrCenter::OadrCenter(MODULE *mod) {
	if (oclass != NULL) {
		exception("OadrCenter: this object has already been registered.");
		return;
	}

	oclass = gld_class::create(mod, "OadrCenter", sizeof(OadrCenter), passconfig|PC_AUTOLOCK);
	if (oclass == NULL) {
		exception("OadrCenter: unable to register the class OadrCenter.");
		return;
	}

    if (gl_publish_variable(
            oclass,
            PT_object, "dbase", get_dbase_offset(), PT_DESCRIPTION, "Database object to be used when accessing persistent data.",
            PT_char256, "plannerName", get_plannerName_offset(), PT_DESCRIPTION, "Algorithm used to transform and distribute the input event.",
            NULL
    ))

	gl_publish_function(oclass, "register_box", (FUNCTIONADDR)register_box);

	oclass->trl = TRL_UNKNOWN;
	defaults = this;

	memset((void*)defaults, 0, sizeof(OadrCenter));
	return;
}


/**
 * ~OadrCenter.
 * 
 * Default destructor.
 */
OadrCenter::~OadrCenter() {
    finish();
    return;
}


/**
 * finish.
 * 
 * Releases the resources before the object is destroyed.
 */
void OadrCenter::finish() {
    if (this->program != NULL) {
        delete this->program;
    }
    return;
}


/**
 * create.
 *
 * The create function is run before user-defined values are loaded and "init" is performed.
 * This event allows you to set up each "object" prior to user values being set by the GLM file.
 * This is the time to set default values and values used to detect whether the user defined
 * required values (e.g., negative or zero that are not valid).
 *
 * The "create" function must return SUCCESS or FAILED to indicate the result. The simulation
 * will stop if the return value is not SUCCESS.
 *
 */
int OadrCenter::create(void) {
	memcpy((void*)this, (void*)defaults, sizeof(*this));
	return SUCCESS;
}


/**
 * init.
 *
 * Object initialization is called once after all object have been created.
 * The values specified by the user at the GLM file are loaded at this time.
 *
 */
int OadrCenter::init(OBJECT *parent) {
	if (strlen(oadrProgramFilePath) == 0) {
		exception("OadrCenter: init: the global parameter 'openadr::program' is not defined.");
		return FAILED;
	}

	// Check that this is the only OadrCenter defined within the scenario.
	if (!checkOadrCenterUniqueness()) {
		exception("OadrCenter: init: an object (only one) of type OadrCenter must be declared.");
		return FAILED;
	}

    // Check that the name of the planner is fulfilled.
    char *plannerName = get_plannerName();
    if (plannerName == NULL) {
        exception("OadrCenter: init: the name of the 'planner' algorithm must be defined.");
    }

	// Load the Oadr program.
	try {
        this->program = oadr::Program::createFromJsonFile(std::string(oadrProgramFilePath));
    } catch (char const * exMsg) {
        gl_output("ERROR: %s", exMsg);
        return FAILED;
    }

    this->simulData.idxNextEvent = 0;

	return SUCCESS;
}

/**
 * checkOadrCenterUniqueness.
 */
bool OadrCenter::checkOadrCenterUniqueness() const {
	FINDLIST *centersList = gl_find_objects(FL_NEW, FT_CLASS, SAME, "OadrCenter", FT_END);
	const int count = centersList->hit_count;
	gl_free(centersList);
	return count == 1;
}

database* OadrCenter::getDatabaseObj() {
    OBJECT* obj = get_dbase();
    if (obj == NULL) {
        return NULL;
    }

    database* db = OBJECTDATA(obj, database);
    if (db == NULL) {
        exception("OadrCenter: init: getDatabaseObj: failed to load the database object.");
        return NULL;
    } else if (!db->isa("database")) {
        exception("OadrCenter: init: getDabaseObj: the object doesn't seem to be of type 'database'?");
        return NULL;
    }

    return db;
}


/**
 * registerOadrBox.
 * 
 * Register a OadrBox object with the OadrCenter.
 */
void OadrCenter::registerOadrBox(OBJECT *obj) {
    this->simulData.boxes.push_back(obj);
    return;
}


/**
 * sync.
 */
TIMESTAMP OadrCenter::sync(TIMESTAMP t0) {
    TIMESTAMP t2 = TS_NEVER;
	return t2; /* return t2>t1 on success, t2=t1 for retry, t2<t1 on failure */
}


/**
 * presync.
 * This is called when the clock needs to advance on the first top-down pass.
 */
TIMESTAMP OadrCenter::presync(TIMESTAMP t0) {
    const oadr::ProgramEvent *nextEvent = findNextEvent(t0);
    if (nextEvent == NULL) {
        return TS_NEVER;
    }

    if (nextEvent->getNotifTime() != t0) { // This stop doesn't match with the start of a notification window.
//        char ds[1024];
//        callback->time.convert_from_timestamp(nextEvent->getNotifTime(), ds, sizeof(ds));
        return nextEvent->getNotifTime();
    }

    // Distribute the event using an instance of CenterPlanner.
    oadr::PlannerData pdata;
    pdata.oCenter = this->my();
    pdata.boxes = &(this->simulData.boxes);
    pdata.event = nextEvent;
    pdata.dbase = getDatabaseObj();
    oadr::CenterPlanner *planner;
    try {
        planner = oadr::CenterPlannerFactory::createCenterPlanner(std::string(plannerName), pdata);
    } catch (char const *exMsg) {
        gl_error("failed to create the CenterPlanner: %s", exMsg);
        return FAILED;
    }

    planner->run();
    oadr::CenterPlannerFactory::free(planner);

    // Set the new stop time.
    this->simulData.idxNextEvent++;
    if (this->simulData.idxNextEvent < program->numberOfEvents()) { // There are more events.
        return program->event(this->simulData.idxNextEvent)->getNotifTime();
    }

	return TS_NEVER;
}


/**
 * postsync.
 * This is called when the clock needs to advance on the second top-down pass.
 */
TIMESTAMP OadrCenter::postsync(TIMESTAMP t0) {
    TIMESTAMP t2 = TS_NEVER;
	return t2;
}

/**
 * findNextEvent.
 * Returns the next event which should be executed assuming that `t` is the current time.
 * All events whose start period is before `t` are therefore discarded.
 */
const oadr::ProgramEvent* OadrCenter::findNextEvent(TIMESTAMP t) {
    if ((program->numberOfEvents() == 0) || (this->simulData.idxNextEvent == program->numberOfEvents())) {
        return NULL;
    }

    const oadr::ProgramEvent *nextEvent = program->event(this->simulData.idxNextEvent);
    while (nextEvent->getNotifTime() < t) {
        this->simulData.idxNextEvent++;
        if (this->simulData.idxNextEvent == program->numberOfEvents()) {
            return NULL;
        }
        nextEvent = program->event(this->simulData.idxNextEvent);
    }

    return nextEvent;
}


/**
 * register_box.
 * This is the mechanism used by OadrBox to register with the OadrCenter.
 * This makes it unnecessary to manually set the OadrCenter rank high in the GLM file and avoids
 * the use of gl_set_dependent during init() of participants.
 */
EXPORT int64 register_box(OBJECT *box, OBJECT *center) {
	if (center == NULL) {
		gl_error("OadrCenter: register_box: Illegal state exception: the OadrCenter instance is not initialized.");
		return -1;
	}
	// promote the OadrCenter above the rank of any OadrBox.
	if (box->rank >= center->rank) {
		gl_set_rank(center, box->rank + 1);
	}
	
	// register the OadrBox with the OadrCenter.
    OadrCenter *oCenter = OBJECTDATA(center, OadrCenter);
    oCenter->registerOadrBox(box);

	return 0;
}
