#ifndef OPENADR_CENTERPLANNERSMART_H
#define OPENADR_CENTERPLANNERSMART_H

#include "CenterPlanner.h"

namespace oadr {

    class CenterPlannerSmart : public CenterPlanner {
    public:
        virtual ~CenterPlannerSmart();
        virtual void run();
        static CenterPlanner* create(PlannerData data);

    protected:
        CenterPlannerSmart(PlannerData data);

    protected:
        static std::string name;
        static const bool registered;

    private:
        std::vector<DbItem>* buildDbItemsArray();
    };
}


#endif //OPENADR_CENTERPLANNERSMART_H
