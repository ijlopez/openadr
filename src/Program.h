#ifndef SRC_PROGRAM_H_
#define SRC_PROGRAM_H_

#include <string>
#include <vector>
#include "ProgramEvent.h"

namespace oadr {

// Forward declaration
class ProgramEvent;

/**
 * Program.
 */
class Program {

public:

    virtual ~Program();
    
    inline std::string getName() const {
        return name;
    }
    
    inline std::string getDescription() const {
        return description;
    }
    
    inline int numberOfEvents() const {
        return events.size();
    }

    const ProgramEvent* event(int) const;

    static Program* createFromJsonFile(std::string filename);

    static Program* createFromJsonString(std::string jsonContent);


private: // private methods
    Program(std::string name, std::string desc, std::vector<ProgramEvent> events);

    static bool readFileContent(std::string filename, std::string &content);
    
    static Program* desarializeJson(std::string filename);

private: // private variables
    std::string name;
    std::string description;
    std::vector<ProgramEvent> events;
};

}

#endif /* SRC_PROGRAM_H_ */
