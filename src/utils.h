#ifndef SRC_UTILS_H_
#define SRC_UTILS_H_

#include <string>
#include <vector>

inline std::vector<std::string>& splitTokens(std::string const &s, char delim, std::vector<std::string> &elems);

long xmlDateToTimestamp(std::string const &xml);

double fahrenheitToCelcius(double f);

double celciusToFahrenheit(double c);

int strEndsWith(const char *str, const char *suffix);

#endif /* SRC_UTILS_H_ */
