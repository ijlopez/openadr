#ifndef SRC_OADRBOX_H_
#define SRC_OADRBOX_H_

#include <vector>
#include "ProgramEvent.h"
#include "EventProcessor.h"
#include <gridlabd.h>
#include <house_e.h>

// Forward declaration of the functions published by the OadrCenter object.
EXPORT int64 register_event(OBJECT *objBox, oadr::ProgramEvent *event);

class OadrBox : public gld_object {

public: // published properties
    GL_ATOMIC(OBJECT*, center);
	GL_ATOMIC(double, heatingSetpoint);
    GL_ATOMIC(double, coolingSetpoint);
	GL_ATOMIC(double, tankSetpoint);

public:
	OadrBox(MODULE *mod);
    ~OadrBox();
	int create(void);
	int init(OBJECT *parent);
    void finish();

	TIMESTAMP presync(TIMESTAMP t);
	TIMESTAMP sync(TIMESTAMP t);
	TIMESTAMP postsync(TIMESTAMP t);

    void applyNormalControl();

    void registerEvent(oadr::ProgramEvent event);

private:
    static inline bool sortStartTime(oadr::ProgramEvent &e1, oadr::ProgramEvent &e2) {
        return e1.getStartTime() < e2.getStartTime();
    }

    TIMESTAMP nextEventStartTime() const;

    void popEvent();

    void initEventProcessorData();

public: // required members
	static CLASS *oclass;
	static OadrBox *defaults;

private:
    oadr::EventProcessor *eventProcessor;
    std::vector<oadr::ProgramEvent> events;
    oadr::EventProcessorData epData;
};

#endif /* SRC_OADRBOX_H_ */
