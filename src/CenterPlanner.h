#ifndef OPENADR_OADRCENTERPLANNER_H
#define OPENADR_OADRCENTERPLANNER_H

#include "OadrCenter.h"
#include "ProgramEvent.h"
#include <database.h>

namespace oadr {

    typedef struct sPlannerData {
        OBJECT* oCenter;
        const ProgramEvent* event;
        std::vector<OBJECT*>* boxes;
        database* dbase;
    } PlannerData;

    typedef struct sDbItem {
        double hvacload;
        std::string name;
        long idx;
    } DbItem;

    class CenterPlanner {
    public:
        virtual ~CenterPlanner();

        virtual void run() = 0;

    protected:
        CenterPlanner(PlannerData data);

        static void defaultRun(PlannerData &data);

    protected:
        PlannerData data;
    };

}

#endif //OPENADR_OADRCENTERPLANNER_H
