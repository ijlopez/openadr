#ifndef OPENADR_CENTERPLANNERBASIC_H
#define OPENADR_CENTERPLANNERBASIC_H

#include "CenterPlanner.h"

namespace oadr {
    class CenterPlannerStandard : public CenterPlanner {
    public:
        virtual ~CenterPlannerStandard();

        virtual void run();

        static CenterPlanner* create(PlannerData data);

    protected:
        CenterPlannerStandard(PlannerData data);

    protected:
        static std::string name;
        static const bool registered;
    };
}

#endif //OPENADR_CENTERPLANNERBASIC_H
