#include "utils.h"
#include <iostream>
#include <sstream>
#include <gridlabd.h>


/**
 * splitTokens splits a string using a particular delimeter.
 */
inline std::vector<std::string>& splitTokens(std::string const &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }

    return elems;
}


/**
 * xmlDateToTimestamp converts a XML date into a timestamp number.
 */
long xmlDateToTimestamp(std::string const &xml) {

    // 'T' marks the end of the date token and the start of the time token.
    size_t idxEnd = xml.find('T');
    if (idxEnd == std::string::npos) {
        return -1;
    }

    DATETIME dt;
    std::vector<std::string> tokens;

    // Fulfill the fields related to the date.
    {
        std::string xmlDatePart = xml.substr(0, idxEnd);
        splitTokens(xmlDatePart, '-', tokens);
        if (tokens.size() != 3) {
            std::cout << "Failed to read three tokens from XML date: there are " << tokens.size() << " tokens" << std::endl;
            return -1;
        }

        // Complete the items related to the date (year, month and day).
        dt.year = atoi(tokens[0].c_str());
        dt.month = atoi(tokens[1].c_str());
        dt.day = atoi(tokens[2].c_str());
    }

    // Fulfill the fields related to the time.
    {
        // it excludes the final letter ('Z').
        tokens.clear();
        std::string xmlTimePart = xml.substr(idxEnd + 1, xml.size() - idxEnd - 1 - 1);
        splitTokens(xmlTimePart, ':', tokens);
        if (tokens.size() != 3) {
        	std::cout << "Failed to read three tokens from XML date: there are " << tokens.size() << " tokens" << std::endl;
            return -1;
        }

        dt.hour = atoi(tokens[0].c_str());
        dt.minute = atoi(tokens[1].c_str());

        std::string secondMicroseconds = tokens[2];
        tokens.clear();
        splitTokens(secondMicroseconds, '.', tokens);
        dt.second = atoi(tokens[0].c_str());
        if (tokens.size() > 1) {
            dt.nanosecond = atoi(tokens[1].c_str());
        }
    }

    dt.tz[0] = 'G'; dt.tz[1] = 'M'; dt.tz[2] = 'T'; dt.tz[3] = '\0';
    dt.is_dst = 1;
    return callback->time.mkdatetime(&dt);
}

/**
 * Convertion from fahrenheit grades to celcius grades.
 */
double fahrenheitToCelcius(double f) {
    return (f - 32) * (5/(double)9);
}


/**
 * Convertion from celcius grades to fahrenheit grades.
 */
double celciusToFahrenheit(double c) {
    return (9/(double)5)*c + 32;
}


/**
 * Check if a string ends with a particular suffix.
 */
int strEndsWith(const char *str, const char *suffix)
{
    if (!str || !suffix) {
        return 0;
    }

    size_t lenstr = strlen(str);
    size_t lensuffix = strlen(suffix);
    if (lensuffix >  lenstr) {
        return 0;
    }

    return strncmp(str + lenstr - lensuffix, suffix, lensuffix) == 0;
}