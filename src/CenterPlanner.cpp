#include "CenterPlanner.h"
#include "OadrBox.h"

namespace oadr {

    /**
     * OadrCenterPlanner.
     * Default constructor.
     */
    CenterPlanner::CenterPlanner(PlannerData data) : data(data) {
        return;
    }


    /**
     * ~OadrCenterPlanner.
     * Default destructor.
     */
    CenterPlanner::~CenterPlanner() {
        return;
    }


    /**
     * basicRun.
     * Sends the message to all the registered boxes without modifying the event.
     */
    void CenterPlanner::defaultRun(PlannerData &data) {
        for (std::vector<OBJECT*>::const_iterator it = data.boxes->begin(); it != data.boxes->end(); it++) {
            OadrBox* box = OBJECTDATA(*it, OadrBox);
            FUNCTIONADDR func_reg_event = box->get_function("register_event");
            func_reg_event(*it, data.event);
        }
        return;
    }
}
