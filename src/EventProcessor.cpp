#include <string.h>
#include <malloc.h>
#include "EventProcessor.h"

namespace oadr {

    /**
     * EventProcessor.
     * Default constructor.
     */
    EventProcessor::EventProcessor() {
        return;
    }


    /**
     * ~EventProcessor.
     * Default destructor.
     */
    EventProcessor::~EventProcessor() {
        return;
    }


    /**
     * EventProcessorData.
     * Default constructor.
     */
    EventProcessorData::EventProcessorData() {
        house = NULL;
        wheater = NULL;
        zipLoads = NULL;
        nZipLoads = 0;
        tankSetpoint = -1;
        coolingSetpoint = -1;
        heatingSetpoint = -1;
        return;
    }


    /**
     * EventProcessorData.
     * Copy constructor.
     */
    EventProcessorData::EventProcessorData(EventProcessorData const&other) {
        house = other.house;
        wheater = other.wheater;
        tankSetpoint = other.tankSetpoint;
        coolingSetpoint = other.coolingSetpoint;
        heatingSetpoint = other.heatingSetpoint;

        nZipLoads = other.nZipLoads;
        zipLoads = NULL;
        if (nZipLoads > 0) {
            zipLoads = (OBJECT**)malloc(sizeof(OBJECT*) * nZipLoads);
            memcpy(zipLoads, other.zipLoads, nZipLoads * sizeof(OBJECT*));
        }

        return;
    }


    /**
     * operator=
     */
    EventProcessorData &EventProcessorData::operator=(const EventProcessorData &other) {
        house = other.house;
        wheater = other.wheater;
        tankSetpoint = other.tankSetpoint;
        coolingSetpoint = other.coolingSetpoint;
        heatingSetpoint = other.heatingSetpoint;

        if (nZipLoads > 0) {
            free(zipLoads);
            zipLoads = NULL;
        }
        nZipLoads = other.nZipLoads;
        if (nZipLoads > 0) {
            zipLoads = (OBJECT**)malloc(sizeof(OBJECT*) * nZipLoads);
            memcpy(zipLoads, other.zipLoads, nZipLoads * sizeof(OBJECT*));
        }

        return *this;
    }

    /**
     * EventProcessorData.
     * Default destructor.
     */
    EventProcessorData::~EventProcessorData() {
        house = NULL;
        wheater = NULL;
        if (nZipLoads > 0) {
            free(zipLoads);
        }
        nZipLoads = 0;
        zipLoads = NULL;
        tankSetpoint = -1;
        coolingSetpoint = -1;
        heatingSetpoint = -1;
        return;
    }
}