#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <time.h>

#include "OadrCenter.h"
#include "OadrBox.h"
#include <gridlabd.h>
#include "globals.h"


/**
 * init.
 */
EXPORT CLASS *init(CALLBACKS *fntable, MODULE *module, int argc, char *argv[]) {
	// set the GridLAB core API callback table
	callback = fntable;

	// Init the seed for random operations.
	srand(time(NULL));

	gl_global_create("openadr::program",
			PT_char1024, &oadrProgramFilePath,
			PT_ACCESS, PA_PUBLIC,
			PT_DESCRIPTION, "Path to the JSON file that defines an OpenAdr program based on simple signals.",
			NULL);

    gl_global_create("openadr::eventProcessor",
                     PT_char1024, &oadrEventProcessorName,
                     PT_ACCESS, PA_PUBLIC,
                     PT_DESCRIPTION, "Name of the event processor used by the OadrBox elements.",
                     NULL
    );

	new OadrBox(module);
	new OadrCenter(module);

	return OadrBox::oclass;
}


/**
 * do_kill.
 */
CDECL int do_kill() {
	// if anything needs to be deleted or freed, this is a good time to do it.
	return 0;
}
