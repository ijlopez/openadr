#include <vector>
#include "CenterPlannerStandard.h"
#include "CenterPlannerFactory.h"
#include "OadrBox.h"


namespace oadr {

    std::string CenterPlannerStandard::name = "standard";
    const bool CenterPlannerStandard::registered =
            CenterPlannerFactory::registerCenterPlanner(name, &CenterPlannerStandard::create);

    /**
     * CenterPlannerBasic.
     * Default constructor.
     */
    CenterPlannerStandard::CenterPlannerStandard(PlannerData data) : CenterPlanner(data) {
        return;
    }


    /**
     * ~CenterPlannerBasic.
     * Default destructor.
     */
    CenterPlannerStandard::~CenterPlannerStandard() {
        return;
    }


    /**
     * run.
     * Implementation of the basic behavior of the OadrCenter. The behavior is reduced to
     * convey the input events as they come.
     */
    void CenterPlannerStandard::run() {
        for (std::vector<OBJECT*>::const_iterator it = data.boxes->begin(); it != data.boxes->end(); it++) {
            OadrBox* box = OBJECTDATA(*it, OadrBox);
            FUNCTIONADDR func_reg_event = box->get_function("register_event");
            func_reg_event(*it, data.event);
        }
        return;
    }


    /**
     * create.
     */
    CenterPlanner *CenterPlannerStandard::create(PlannerData data) {
        return new CenterPlannerStandard(data);
    }
}