
#ifndef OPENADR_EVENTPROCESSORHVAC_H_H
#define OPENADR_EVENTPROCESSORHVAC_H_H

#include "EventProcessor.h"

namespace oadr {
    class EventProcessorHvac : public EventProcessor {
    public:
        virtual ~EventProcessorHvac();

        virtual void process(const ProgramEvent &event, const EventProcessorData &data);

        virtual void stop(const ProgramEvent &event, const EventProcessorData &data);

        static EventProcessor *create();

    protected:
        EventProcessorHvac();

    protected:
        static std::string name;
        static const bool registered;
    };
}

#endif //OPENADR_EVENTPROCESSORHVAC_H_H
