#include <algorithm>
#include <vector>
#include "CenterPlannerUniform.h"
#include "CenterPlannerFactory.h"
#include "OadrBox.h"

namespace oadr {
    std::string CenterPlannerUniform::name = "uniform";
    const bool CenterPlannerUniform::registered =
            CenterPlannerFactory::registerCenterPlanner(name, &CenterPlannerUniform::create);

    CenterPlannerUniform::CenterPlannerUniform(PlannerData data) : CenterPlanner(data) {
        return;
    }

    CenterPlannerUniform::~CenterPlannerUniform() {
        return;
    }

    void CenterPlannerUniform::run() {
        if (data.event->getStartAfter() == 0) {
            gl_warning("CenterPlannerUniform: run: startafter is equal to 0 so the default planner is used.");
            defaultRun(data);
            return;
        }

        std::random_shuffle(data.boxes->begin(), data.boxes->end());
        const float offsetFactor = data.event->getStartAfter() /(float)data.boxes->size();
        int i = 0;
        for (std::vector<OBJECT*>::const_iterator it = data.boxes->begin(); it != data.boxes->end(); it++,i++) {
            const long newStartTime = data.event->getStartTime() + (long)(i*offsetFactor);
            oadr::ProgramEvent boxEvent(
                    newStartTime,
                    data.event->getNotifDuration(),
                    data.event->getDuration(),
                    data.event->getRecoveryDuration(),
                    data.event->getLevel(),
                    0);

            OadrBox* box = OBJECTDATA(*it, OadrBox);
            FUNCTIONADDR func_reg_event = box->get_function("register_event");
            func_reg_event(*it, &boxEvent);
        }


        return;
    }

    CenterPlanner* CenterPlannerUniform::create(PlannerData data) {
        return new CenterPlannerUniform(data);
    }
}