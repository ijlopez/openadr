#include <zipload.h>
#include "EventProcessorZips.h"
#include "EventProcessorFactory.h"
#include "utils.h"

namespace oadr {
    const float DELTA_WATERHEATER_MODERATE = 3;
    const float DELTA_WATERHEATER_HIGH = 5;

    // Definition of the static declarations
    std::string EventProcessorZips::name = "zips";
    const bool EventProcessorZips::registered =
            EventProcessorFactory::registerProcessor(name, &EventProcessorZips::create);

    /**
     * constructor.
     */
     EventProcessorZips::EventProcessorZips() {
        return;
    }


    /**
     * Default destructor.
     */
     EventProcessorZips::~EventProcessorZips() {
        return;
    }


    /**
     * create.
     */
     EventProcessor* EventProcessorZips::create() {
        return new EventProcessorZips();
    }


    /**
     * process.
     */
     void EventProcessorZips::process(const ProgramEvent &event, const EventProcessorData &data) {

        const OpenadrLevel level = event.getLevel();
        if (level == OADR_NORMAL) {
            return;
        }

        for (int n = 0; n < data.nZipLoads; n++) {
            ZIPload* load = OBJECTDATA(data.zipLoads[n], ZIPload);
            if (level == OADR_CRITICAL) {
                load->pCircuit->status = BRK_OPEN;
            } else if (level == OADR_HIGH) {
                if ((strEndsWith(load->get_name(), "tv") == 0) || (strEndsWith(load->get_name(), "lights") == 0)) {
                    load->pCircuit->status = BRK_OPEN;
                }
            } else if (level == OADR_MODERATE) {
                if (strEndsWith(load->get_name(), "tv") == 0) {
                    load->pCircuit->status = BRK_OPEN;
                }
            }

        }

        return;
    }


    /**
     * stop.
     */
     void EventProcessorZips::stop(const ProgramEvent &event, const EventProcessorData &data) {
        // All zip loads are re-connected.
        for (int n = 0; n < data.nZipLoads; n++) {
            ZIPload* load = OBJECTDATA(data.zipLoads[n], ZIPload);
            load->pCircuit->status = BRK_CLOSED;
        }

        return;
    }
}
