#include <iostream>
#include <fstream>
#include <algorithm>
#include "Program.h"
#include "globals.h"

namespace oadr {

/**
 * Program::Program() is the default constructor.
 */
Program::Program(std::string name, std::string desc, std::vector<ProgramEvent> events) : name(name), description(desc) {
    std::vector<ProgramEvent>::iterator it;
    for (it = events.begin(); it < events.end(); it++) {
        this->events.push_back(*it);
    }
    return;
}


/**
 * ~Program().
 * 
 * Default destructor.
 */
Program::~Program() {
    return;
}


/**
 *
 */
const ProgramEvent* Program::event(int idx) const {
    return &(events[idx]);
}

/**
 * createFromJsonString.
 *
 * This creates a new Program entity from a JSON string. 
 */
Program* Program::createFromJsonString(std::string jsonContent) {
    Program *p = NULL;
    try {
        p = desarializeJson(jsonContent.c_str());
    } catch (char const * exMsg) {
        sprintf(oadrMsg, "Program: createFromJsonString: Failed to createEventProcessor Program from JSON: %s", exMsg);
        throw oadrMsg;
    }

    // Sort the program events according to the start time.
    std::sort(
        p->events.begin(),
        p->events.end(),
        ProgramEvent::sortNotifTime);

    return p;
}


/**
 * createFromJsonFile.
 * 
 * 
 */
Program* Program::createFromJsonFile(std::string filename) {
    std::string content;
    if (!readFileContent(filename, content)) {
        throw "Program: createFromJsonFile: failed to read the JSON file";
    }

    return createFromJsonString(content);
}


/**
 * Program::readFileContent reads the content of a file and returns it.
 */
bool Program::readFileContent(std::string filename, std::string &content) {
    std::ifstream jsonFile(filename.c_str());
    if (!jsonFile.is_open()) {
        return false;
    }

    std::string line;
    while (std::getline(jsonFile, line)) {
        content.append(line);
    }
    jsonFile.close();

    return true;
}


/**
 * Program::deserializeJson builds a Program object from a JSON string.
 */
Program* Program::desarializeJson(std::string jsonContent) {
    json_object* jsonObj = json_tokener_parse(jsonContent.c_str());
    if (jsonObj == NULL) {
        throw "Invalid JSON content.";
    } else if (!json_object_is_type(jsonObj, json_type_object)) {
        json_object_put(jsonObj);
        throw "The content is not a JSON object.";
    }
    
    std::string name;
    std::string desc;
    std::vector<ProgramEvent> events;
    struct json_object_iterator it = json_object_iter_begin(jsonObj);
    struct json_object_iterator itEnd = json_object_iter_end(jsonObj);
    while (!json_object_iter_equal(&it, &itEnd)) {
        std::string key = std::string(json_object_iter_peek_name(&it));
        if (key.compare("name") == 0) {
            name = json_object_get_string(json_object_iter_peek_value(&it));
        } else if (key.compare("description") == 0) {
            desc = json_object_get_string(json_object_iter_peek_value(&it));
        } else if (key.compare("events") == 0) {
            json_object *jsonEvents = json_object_iter_peek_value(&it);

            if (json_object_is_type(jsonEvents, json_type_null)) {
                json_object_iter_next(&it);
                continue;
            }

            if (!json_object_is_type(jsonEvents, json_type_array)) {
                throw "the 'events' element is not an array";
            }

            array_list* jsonArray = json_object_get_array(jsonEvents);
            for (int i = 0; i < jsonArray->length; i++) {
                try {
                    ProgramEvent event = ProgramEvent::buildFromJson(json_object_array_get_idx(jsonEvents, i));
                    events.push_back(event);
                } catch (char const *exMsg) {
                    json_object_put(jsonObj);
                    throw exMsg;
                }
            }
        }
        json_object_iter_next(&it);
    }
    
    json_object_put(jsonObj);
    
    return new Program(name, desc, events);
}

}
