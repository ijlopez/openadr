#ifndef OPENADR_EVENTPROCESSORLHW_H
#define OPENADR_EVENTPROCESSORLHW_H

#include <string>
#include "EventProcessor.h"

namespace oadr {
    class EventProcessorZips : public EventProcessor {

    public:
        virtual ~EventProcessorZips();

        virtual void process(const ProgramEvent &event, const EventProcessorData &data);

        virtual void stop(const ProgramEvent &event, const EventProcessorData &data);

        static EventProcessor *create();

    protected:
        EventProcessorZips();

    protected:
        static std::string name;
        static const bool registered;
    };
}


#endif //OPENADR_EVENTPROCESSORLHW_H
