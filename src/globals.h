#ifndef SRC_GLOBALS_H_
#define SRC_GLOBALS_H_

#include <gridlabd.h>

extern char1024 oadrProgramFilePath; /** Path to the JSON file defining the program. */
extern char1024 oadrEventProcessorName; /** Name of the Event Processor. */
extern char1024 oadrMsg;



#endif /* SRC_GLOBALS_H_ */
