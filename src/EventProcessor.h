#ifndef OPENADR_EVENTPROCESSOR_H
#define OPENADR_EVENTPROCESSOR_H

#include "ProgramEvent.h"
#include <gridlabd.h>
#include <house_e.h>
#include <waterheater.h>

namespace oadr {

    /**
     * EventProcessorData.
     */
    class EventProcessorData {
    public:
        EventProcessorData();
        EventProcessorData(EventProcessorData const &);
        EventProcessorData& operator=(const EventProcessorData& other);
        ~EventProcessorData();

    public:
        house_e* house;
        waterheater* wheater;
        OBJECT** zipLoads;
        int nZipLoads;
        double tankSetpoint;
        double coolingSetpoint;
        double heatingSetpoint;
    };


    /**
     * EventProcessor.
     */
    class EventProcessor {
    public:
        virtual ~EventProcessor();

        virtual void process(const ProgramEvent &event, const EventProcessorData &data) = 0;

        virtual void stop(const ProgramEvent &event, const EventProcessorData &data) = 0;

    protected:
        EventProcessor();
    };
}


#endif //OPENADR_EVENTPROCESSOR_H
