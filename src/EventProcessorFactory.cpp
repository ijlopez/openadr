#include "EventProcessorFactory.h"

namespace oadr {

    ProcessorsMap EventProcessorFactory::processors;

    /**
     * createEventProcessor.
     * This returns an instance of EventProcessor according to the name (type) specified as parameter.
     */
    EventProcessor* EventProcessorFactory::createEventProcessor(std::string name) {
        ProcessorsMap::iterator it = processors.find(name);
        if (it == processors.end()) {
            throw "processor not registered";
        }

        EventProcessorCreateFn fn = (*it).second;
        return fn();
    }


    /**
     * free.
     */
     void EventProcessorFactory::free(EventProcessor *processor) {
        if (processor == NULL) {
            return;
        }

        delete processor;
        return;
    }


    /**
     * registerProcessor.
     * This registers a new method for creating EventProcessor entities.
     */
    bool EventProcessorFactory::registerProcessor(const std::string name, EventProcessorCreateFn pfnCreate) {
        processors[name] = pfnCreate;
        return true;
    }
}