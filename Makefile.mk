pkglib_LTLIBRARIES += openadr/openadr.la

openadr_openadr_la_CPPFLAGS =
openadr_openadr_la_CPPFLAGS += $(AM_CPPFLAGS)

openadr_openadr_la_LIBADD = -ljson-c -lmysqlclient

AM_CPPFLAGS += -I$(top_srcdir)/residential
AM_CPPFLAGS += -I$(top_srcdir)/mysql

openadr_openadr_la_SOURCES =
openadr_openadr_la_SOURCES += openadr/src/main.cpp
openadr_openadr_la_SOURCES += openadr/src/init.cpp
openadr_openadr_la_SOURCES += openadr/src/globals.h openadr/src/globals.cpp
openadr_openadr_la_SOURCES += openadr/src/utils.h openadr/src/utils.cpp
openadr_openadr_la_SOURCES += openadr/src/ProgramEvent.h openadr/src/ProgramEvent.cpp
openadr_openadr_la_SOURCES += openadr/src/Program.h openadr/src/Program.cpp
openadr_openadr_la_SOURCES += openadr/src/CenterPlannerFactory.h openadr/src/CenterPlannerFactory.cpp
openadr_openadr_la_SOURCES += openadr/src/CenterPlanner.h openadr/src/CenterPlanner.cpp
openadr_openadr_la_SOURCES += openadr/src/CenterPlannerStandard.h openadr/src/CenterPlannerStandard.cpp
openadr_openadr_la_SOURCES += openadr/src/CenterPlannerUniform.h openadr/src/CenterPlannerUniform.cpp
openadr_openadr_la_SOURCES += openadr/src/CenterPlannerSmart.h openadr/src/CenterPlannerSmart.cpp
openadr_openadr_la_SOURCES += openadr/src/CenterPlannerOrder.h openadr/src/CenterPlannerOrder.cpp
openadr_openadr_la_SOURCES += openadr/src/EventProcessor.h openadr/src/EventProcessor.cpp
openadr_openadr_la_SOURCES += openadr/src/EventProcessorFactory.h openadr/src/EventProcessorFactory.cpp
openadr_openadr_la_SOURCES += openadr/src/EventProcessorZips.h openadr/src/EventProcessorZips.cpp
openadr_openadr_la_SOURCES += openadr/src/EventProcessorHvac.h openadr/src/EventProcessorHvac.cpp
openadr_openadr_la_SOURCES += openadr/src/EventProcessorNil.h openadr/src/EventProcessorNil.cpp
openadr_openadr_la_SOURCES += openadr/src/OadrCenter.h openadr/src/OadrCenter.cpp
openadr_openadr_la_SOURCES += openadr/src/OadrBox.h openadr/src/OadrBox.cpp